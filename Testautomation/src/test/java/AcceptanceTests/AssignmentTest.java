package AcceptanceTests;

import java.util.List;
import java.util.Map;

import org.testng.annotations.Test;
import static org.testng.Assert.assertEquals;

import RestAPIClient.RestApiClass;
import io.restassured.response.Response;

public class AssignmentTest {

	@Test()
	public void acceptanceTest() {
		Response res = RestApiClass
				.getApiResponse("https://api.tmsandbox.co.nz/v1/Categories/6327/Details.json?catalogue=false");
		
		//Assert the status code
		assertEquals(200, res.getStatusCode());
		
		//Assert the acceptance criteria - Name and CanRelist parameter values 
		assertEquals(res.jsonPath().get("Name"), "Carbon credits", "Name is not same as Carbon credits");
		assertEquals(res.jsonPath().getBoolean("CanRelist"), true, "CanRelist value is false");

		//Get all the Promotion array names
		List<String> promotionNames = res.jsonPath().getList("Promotions.Name");

		assertEquals(promotionNames.size() > 1, true, "Response does not contain promotions array");
		
		//Get the Promotions element
		Map<String, String> promotion = res.jsonPath().setRoot("Promotions").
										get("find {p -> p.Name== /Gallery/}");

		//Assert the description parameter of the promotions json array
		assertEquals(promotion.get("Description").toString().contains("2x larger image in desktop site search results"),
				true, "The description does not contain the expected text");

	}

}
