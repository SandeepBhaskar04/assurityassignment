package RestAPIClient;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.parsing.Parser;
import io.restassured.response.Response;

import static io.restassured.RestAssured.*;

public class RestApiClass {
	
	public static Response getApiResponse(String url) {
		
		RestAssured.defaultParser = Parser.JSON;
		
		return given().headers("Content-Type",ContentType.JSON,
				"Accept",ContentType.JSON).when().get(url).
				then().contentType(ContentType.JSON).extract().response();
		
	}

}
