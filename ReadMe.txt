************************* Important Notes on the Project ******************************************

1) The assignment is developed using Java and RestAssured libraries
2) The project is created as a maven project and all the dependencies are added in POM.xml
3) Using TestNg  as xUnit tests

***************** Steps to run the tests *******************

1) Import this project as a maven project in IDE - Eclipse / intelliJ. (I have used eclipse)
2) Add the TestNg required plugin to run the tests from IDE
3) Run as -> Maven build.. -> clean install
4) The above step should execute the tests as well.
5) To execute the test alone separately - Right click on the test method in the class and select run as TestNg tests.

Note: Point 5) has a dependency on point 2).